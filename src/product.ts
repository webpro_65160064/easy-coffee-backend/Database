import { addAbortSignal } from "stream";
import { AppDataSource } from "./data-source";
import { Product } from "./entity/Product";
import { Type } from "./entity/Type";

AppDataSource.initialize()
  .then(async () => {
    const typesRepository = AppDataSource.getRepository(Type);
    const drinkType = await typesRepository.findOneBy({ id: 1 });
    const bekeryType = await typesRepository.findOneBy({ id: 2 });
    const foodType = await typesRepository.findOneBy({ id: 3 });

    const productsRepository = AppDataSource.getRepository(Product);
    await productsRepository.clear();
    var product = new Product();
    product.id = 1;
    product.name = "Latte";
    product.price = 40;
    product.type = drinkType;
    await productsRepository.save(product);

    var product = new Product();
    product.id = 2;
    product.name = "Americano";
    product.price = 40;
    product.type = drinkType;
    await productsRepository.save(product);

    var product = new Product();
    product.id = 3;
    product.name = "Thai Tea";
    product.price = 40;
    product.type = drinkType;
    await productsRepository.save(product);

    var product = new Product();
    product.id = 4;
    product.name = "Cake";
    product.price = 50;
    product.type = bekeryType;
    await productsRepository.save(product);

    var product = new Product();
    product.id = 5;
    product.name = "Brownie";
    product.price = 25;
    product.type = bekeryType;
    await productsRepository.save(product);

    var product = new Product();
    product.id = 6;
    product.name = "Pizza";
    product.price = 200;
    product.type = foodType;
    await productsRepository.save(product);

    const products = await productsRepository.find({
      relations: { type: true },
    });
    console.log(products);
  })
  .catch((error) => console.log(error));
